FROM node:12

WORKDIR /srv/app

COPY package.json yarn.lock ./
RUN yarn install
RUN yarn build
COPY --chown=node:node . .

CMD ["npm","start"]
